<?php

namespace Drupal\allow_user_access_per_roles\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;

/**
 * Configure Allow user access per role settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'allow_user_access_per_roles.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'allow_user_access_per_roles_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config(static::SETTINGS);
    $user_roles = Role::loadMultiple();
    $roles = [];
    foreach ($user_roles as $key => $value) {
      if ($key != 'anonymous' && $key != 'administrator' && $key != 'authenticated') {
        $roles[$key] = ucwords($key);
      }
    }
    $default_role = $config->get('role_specific_user');

    $form['role_specific_user'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select Role'),
      '#description' => $this->t('Choosing Role will allow all user related to this role.'),
      '#options' => $roles,
      '#default_value' => $default_role,
    ];

    $role_err_message = $config->get('role_specific_user_err_message');
    $form['role_specific_user_err_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message'),
      '#description' => $this->t('Enter error message to show when user login.'),
      '#required' => TRUE,
      '#default_value' => $role_err_message,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('role_specific_user', $form_state->getValue('role_specific_user'))
      ->set('role_specific_user_err_message', $form_state->getValue('role_specific_user_err_message'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
