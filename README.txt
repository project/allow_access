INTRODUCTION
------------
Allow Access module is a simple module that allow to restrcit CMS access
or login only to specific user roles.


REQUIREMENTS
------------
This module requires no modules outside of Drupal core.

INSTALLATION
------------

Install the Allow access module as you would normally install a contributed
Drupal module.

CONFIGURATION
------------
Configure user permissions in Administration -> People -> Permissions

MAINTAINERS
------------
Current maintainers:
RAJEEV C P https://www.drupal.org/u/rajeev-cp
  email : rajeevcp92@gmail.com